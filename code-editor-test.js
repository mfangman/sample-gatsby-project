  /* $state = getGETPOSTsettingvalue('get/post parameter name', null);
  * $state = getGETPOSTsettingvalue('get/post parameter name', false);
  */
function getGETPOSTsettingvalue($settingname, $default) {
    $settingvalue = $default;
	   if (   $_GET[$settingname]) {
        $settingvalue =    $_GET[$settingname];
	   }
	   if ($_POST[$settingname]) {
		      $settingvalue = $_POST[$settingname];
	   }
	   return $settingvalue;
}